Rails.application.routes.draw do
  get 'entertainments/index'

  get 'entertainments/show'
  resources :entertainments
  root 'page#index'
  resources :sub_quizzes
  resources :quizzes
  resources :vocabularies do
    resources :vocabulary_examples
    resources :quizzes
  end
  resources :pronunciation_examples do
    resources :quizzes
  end

  resources :pronunciations 
    # resources :pronunciation_examples
  
  resources :subjects do
   resources :parts
    resources :quizzes
  end
   resources :parts do
      resources :details
    end
  # resources :details
  # resources :parts

  # resources :vocabulary_examples do 
  #   resources :quizzes
  # end
  # get 'sub_quizzes/index'

  # get 'quizzes/index'

  # get 'vocabularies/index'

  # get 'vocabulary_examples/index/'

  # get 'pronunciation_examples/index/'

  # get 'pronunciations/index'

  # get 'parts/index'

  # get 'details/index'

  # get 'subjects/index'

  # get 'page/index'
  # get 'page/vocabulary'
  #  get 'page/itemVocabulary'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
 

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
