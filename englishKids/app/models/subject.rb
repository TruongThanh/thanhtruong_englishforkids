class Subject < ActiveRecord::Base
	has_many :parts
	has_many :quizzes
end
