class Quiz < ActiveRecord::Base
	has_many :sub_quizzes
	belongs_to :subject
	belongs_to :pronunciation
	belongs_to :vocabulary
end
