class Part < ActiveRecord::Base
	belongs_to :subject
	has_many :details
end
