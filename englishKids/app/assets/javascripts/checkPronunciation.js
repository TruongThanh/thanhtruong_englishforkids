// // /* Home page - alert */
$(document).ready(function() {

var local = window.location.href;
var regex_Vocabulary = new RegExp("http://localhost:3000/vocabularies", "gi"); 
var regex_Subject = new RegExp("http://localhost:3000/subjects", "gi"); 
var regex_Part = new RegExp("http://localhost:3000/parts", "gi"); 
var regex_Pronunciation = new RegExp("http://localhost:3000/pronunciations", "gi"); 
var regex_Pronunciation_examples = new RegExp("http://localhost:3000/pronunciation_examples", "gi"); 
var regex_sub_quizzes = new RegExp("http://localhost:3000/sub_quizzes", "gi"); 
var regex_entertainment = new RegExp("http://localhost:3000/entertainments", "gi"); 
if((regex_Vocabulary.test(local) == true)){
	document.getElementById('btn-vocabulary').style.background = 'rgb(255, 223, 1)';
}
if((regex_Pronunciation.test(local) == true) || (regex_Pronunciation_examples.test(local) == true) || (regex_sub_quizzes.test(local) == true)){
	document.getElementById('btn-pronunciation').style.background = 'rgb(255, 223, 1)';
}
if((regex_Subject.test(local) == true) || (regex_Part.test(local) == true)){
	document.getElementById('btn-subject').style.background = 'rgb(255, 223, 1)';
}
if((regex_entertainment.test(local) == true)){
	document.getElementById('btn-entertainment').style.background = 'rgb(255, 223, 1)';
}
});
