class VocabularyExamplesController < ApplicationController
	layout 'layout'
  def index
  	@vocabulary = Vocabulary.find(params[:vocabulary_id])
    @vocabulary_examples = @vocabulary.vocabulary_examples.all.paginate(:page => params[:page], per_page: 3)
    # gon.vocabulary_examples = VocabularyExample.all
    # gon.details = Detail.all
    @items = []
    @vocabulary_examples.map do |e|
      obj = {
        index: e.index,
        id: e.id,
      }
      @items << obj
      # <p id="item" class="hidden"><%= @items.to_json%>
  end
end
  def show
  	
  end
end
