class SubQuizzesController < ApplicationController
	layout 'layout'
  def index
  end
  def show
  	@pronunciation = Pronunciation.find(params[:id])
  	@quizzes = @pronunciation.quizzes.all
  	@subquizzes =SubQuiz.all
  	# @sub_quizzes=@quizzes.sub_quizzes.all
  	@item_pronunciation = []
    @quizzes.map do |e|
      obj = {
        id: e.id,
        check: e.check,
      }
      @item_pronunciation << obj
    end
  end
end
