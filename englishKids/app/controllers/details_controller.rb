class DetailsController < ApplicationController
	layout 'layout'
  def index
  	@part = Part.find(params[:part_id])
    @details = @part.details.all.paginate(:page => params[:page], per_page: 3)
     @item_details = []
	    @details.map do |e|
	      obj = {
	        index: e.index,
	        id: e.id,
	      }
	      @item_details << obj
	  	end
  end
  
  def show
  	@part = Part.find(params[:part_id])
    @details = @part.details.all.paginate(:page => params[:page], per_page: 1)
  end
end
