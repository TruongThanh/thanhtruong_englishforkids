class EntertainmentsController < ApplicationController
	layout 'layout'
  def index
  	@entertainments = Entertainment.where(:role => "Song").all
  	# @quizzes = Quiz.where(:vocabulary_id => params[:id]).all
  	@sub_entertainments = SubEntertainment.all
  	@event = []
    @entertainments.map do |e|
      obj = {
        name: e.name,
        id: e.id,
        role: e.role,
      }
      @event << obj
    end
  end

  def show
  end
end
