class QuizzesController < ApplicationController
	layout 'layout'
  def index
  	@vocabulary = Vocabulary.find(params[:vocabulary_id])
  	@quizzes = @vocabulary.quizzes.all.paginate(:page => params[:page], per_page: 1)
  	# @quizzes = Quiz.where(:vocabulary_id => params[:id]).all.paginate(:page => params[:page], per_page: 1)
  	# gon.quizzes = Quiz.where(:vocabulary_id => params[:id]).all
  	@sub_quizzes = SubQuiz.all
  	# gon.sub_quizzes = SubQuiz.all
    @item_quiz = []
    @quizzes.map do |e|
      obj = {
        id: e.id,
        check: e.check,
      }
      @item_quiz << obj
    end
  end
  def show
  	 @subject = Subject.find(params[:subject_id])
    @quizzes = @subject.quizzes.all.paginate(:page => params[:page], per_page: 1)
    @sub_quizzes = SubQuiz.all
    @item_quiz = []
    @quizzes.map do |e|
      obj = {
        id: e.id,
        check: e.check,
      }
      @item_quiz << obj
    end
  end
end
