class PronunciationsController < ApplicationController
	layout 'layout'
  def index
  	@short_vowels = Pronunciation.where(:role => 'short vowel').all
  	@long_vowels = Pronunciation.where(:role => 'long vowel').all
  	@diphthong_vowels = Pronunciation.where(:role => 'diphthong vowel').all
  	@consonant_vowels = Pronunciation.where(:role => 'consonant vowel').all
  	# @pronunciation_example = PronunciationExample.find(params[:pronunciation_id])
  	# @pronunciation_example = PronunciationExample.find(:id => 1)
    @compare_sounds = Pronunciation.where(:role => 'compare sound').all
    @t = 'pro'
    
  end
  def show
  end
end
