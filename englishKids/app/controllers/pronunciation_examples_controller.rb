class PronunciationExamplesController < ApplicationController
  layout 'layout'
  def index
    @pronunciation = Pronunciation.find(params[:format])
    @quiz = Quiz.where(:pronunciation_id => params[:format])
  	@compareSounds = PronunciationExample.where(:pronunciation_id => params[:format])
    @pronunciations = []
    @compareSounds.map do |e|
      obj = {
        index: e.index,
        id: e.id,
      }
      @pronunciations << obj
    end
      # @compareSoundright = PronunciationExample.where(:pronunciation_id => params[:format], :position => '2')
  end
  def show
  	@pronunciation = Pronunciation.find(params[:id])
    @quiz = Quiz.where(:pronunciation_id => params[:id])
  	@tmps = PronunciationExample.where(:pronunciation_id => params[:id]).all.paginate(:page => params[:page], per_page: 3)
  	@item_pronunciations = []
    @tmps.map do |e|
      obj = {
        index: e.index,
        id: e.id,
      }
      @item_pronunciations << obj
  end
  end
end
