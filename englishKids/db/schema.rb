# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150414082307) do

  create_table "details", force: :cascade do |t|
    t.string   "index",      limit: 255
    t.string   "position",   limit: 255
    t.string   "source",     limit: 255
    t.string   "img",        limit: 255
    t.string   "translate",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "part_id",    limit: 4
    t.string   "word",       limit: 255
  end

  create_table "entertainments", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "source",     limit: 255
    t.string   "img",        limit: 255
    t.string   "content",    limit: 255
    t.string   "translate",  limit: 255
    t.string   "role",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "parts", force: :cascade do |t|
    t.string   "index",      limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "subject_id", limit: 4
  end

  create_table "pronunciation_examples", force: :cascade do |t|
    t.string   "index",            limit: 255
    t.string   "position",         limit: 255
    t.string   "word",             limit: 255
    t.string   "translate",        limit: 255
    t.string   "vocalize",         limit: 255
    t.string   "img",              limit: 255
    t.string   "source",           limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "pronunciation_id", limit: 4
  end

  create_table "pronunciations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "img",        limit: 255
    t.string   "content",    limit: 255
    t.string   "role",       limit: 255
    t.string   "source",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "quizzes", force: :cascade do |t|
    t.string   "index",            limit: 255
    t.string   "question",         limit: 255
    t.string   "img",              limit: 255
    t.string   "source",           limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "check",            limit: 255
    t.integer  "subject_id",       limit: 4
    t.integer  "pronunciation_id", limit: 4
    t.integer  "vocabulary_id",    limit: 4
  end

  create_table "sub_entertainments", force: :cascade do |t|
    t.string   "index",            limit: 255
    t.string   "content",          limit: 255
    t.string   "translate",        limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "entertainment_id", limit: 4
  end

  create_table "sub_quizzes", force: :cascade do |t|
    t.string   "index",      limit: 255
    t.string   "answer",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "quiz_id",    limit: 4
  end

  create_table "subjects", force: :cascade do |t|
    t.string   "index",      limit: 255
    t.string   "topic",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "img",        limit: 255
  end

  create_table "vocabularies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "img",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "vocabulary_examples", force: :cascade do |t|
    t.string   "index",         limit: 255
    t.string   "word",          limit: 255
    t.string   "translate",     limit: 255
    t.string   "img",           limit: 255
    t.string   "source",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "vocabulary_id", limit: 4
  end

end
