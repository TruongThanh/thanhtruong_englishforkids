class CreateEntertainments < ActiveRecord::Migration
  def change
    create_table :entertainments do |t|
      t.string :name
      t.string :source
      t.string :img
      t.string :content
      t.string :translate
      t.string :role

      t.timestamps null: false
    end
  end
end
