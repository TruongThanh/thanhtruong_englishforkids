class CreatePronunciations < ActiveRecord::Migration
  def change
    create_table :pronunciations do |t|
      t.string :name
      t.string :img
      t.string :content
      t.string :role
      t.string :source

      t.timestamps null: false
    end
  end
end
