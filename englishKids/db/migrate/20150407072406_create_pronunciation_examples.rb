class CreatePronunciationExamples < ActiveRecord::Migration
  def change
    create_table :pronunciation_examples do |t|
      t.string :index
      t.string :position
      t.string :word
      t.string :translate
      t.string :vocalize
      t.string :img
      t.string :source

      t.timestamps null: false
    end
  end
end
