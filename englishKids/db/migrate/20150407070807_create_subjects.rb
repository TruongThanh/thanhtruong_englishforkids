class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :index
      t.string :topic
      t.string :img

      t.timestamps null: false
    end
  end
end
