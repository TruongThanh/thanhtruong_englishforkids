class AddQuizIdToSubQuizzes < ActiveRecord::Migration
  def change
    add_column :sub_quizzes, :quiz_id, :integer
  end
end
