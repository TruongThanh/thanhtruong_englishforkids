class AddSubjectIdToParts < ActiveRecord::Migration
  def change
    add_column :parts, :subject_id, :integer
  end
end
