class AddCheckToQuizzes < ActiveRecord::Migration
  def change
    add_column :quizzes, :check, :string
  end
end
