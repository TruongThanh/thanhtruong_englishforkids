class AddVocabularyIdToVocabularyExamples < ActiveRecord::Migration
  def change
    add_column :vocabulary_examples, :vocabulary_id, :integer
  end
end
