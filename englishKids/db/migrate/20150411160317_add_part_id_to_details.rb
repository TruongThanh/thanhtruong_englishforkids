class AddPartIdToDetails < ActiveRecord::Migration
  def change
    add_column :details, :part_id, :integer
  end
end
