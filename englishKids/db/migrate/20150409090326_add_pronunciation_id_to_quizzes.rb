class AddPronunciationIdToQuizzes < ActiveRecord::Migration
  def change
    add_column :quizzes, :pronunciation_id, :integer
  end
end
