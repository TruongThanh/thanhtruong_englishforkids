class CreateQuizzes < ActiveRecord::Migration
  def change
    create_table :quizzes do |t|
      t.string :index
      t.string :question
      t.string :img
      t.string :source
      t.string :check
      t.timestamps null: false
    end
  end
end
