class AddEntertainmentIdToSubEntertainments < ActiveRecord::Migration
  def change
    add_column :sub_entertainments, :entertainment_id, :integer
  end
end
