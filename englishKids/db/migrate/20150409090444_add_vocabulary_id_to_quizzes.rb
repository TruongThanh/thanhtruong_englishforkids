class AddVocabularyIdToQuizzes < ActiveRecord::Migration
  def change
    add_column :quizzes, :vocabulary_id, :integer
  end
end
