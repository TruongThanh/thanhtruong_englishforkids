class CreateSubEntertainments < ActiveRecord::Migration
  def change
    create_table :sub_entertainments do |t|
      t.string :index
      t.string :content
      t.string :translate

      t.timestamps null: false
    end
  end
end
