require 'test_helper'

class TodoappsControllerTest < ActionController::TestCase
  setup do
    @todoapp = todoapps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:todoapps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create todoapp" do
    assert_difference('Todoapp.count') do
      post :create, todoapp: { notes: @todoapp.notes, title: @todoapp.title }
    end

    assert_redirected_to todoapp_path(assigns(:todoapp))
  end

  test "should show todoapp" do
    get :show, id: @todoapp
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @todoapp
    assert_response :success
  end

  test "should update todoapp" do
    patch :update, id: @todoapp, todoapp: { notes: @todoapp.notes, title: @todoapp.title }
    assert_redirected_to todoapp_path(assigns(:todoapp))
  end

  test "should destroy todoapp" do
    assert_difference('Todoapp.count', -1) do
      delete :destroy, id: @todoapp
    end

    assert_redirected_to todoapps_path
  end
end
