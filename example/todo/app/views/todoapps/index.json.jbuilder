json.array!(@todoapps) do |todoapp|
  json.extract! todoapp, :id, :title, :notes
  json.url todoapp_url(todoapp, format: :json)
end
