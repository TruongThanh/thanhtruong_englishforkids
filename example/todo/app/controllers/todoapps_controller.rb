class TodoappsController < ApplicationController
  before_action :set_todoapp, only: [:show, :edit, :update, :destroy]

  # GET /todoapps
  # GET /todoapps.json
  def index
    @todoapps = Todoapp.all
  end

  # GET /todoapps/1
  # GET /todoapps/1.json
  def show
  end

  # GET /todoapps/new
  def new
    @todoapp = Todoapp.new
  end

  # GET /todoapps/1/edit
  def edit
  end

  # POST /todoapps
  # POST /todoapps.json
  def create
    @todoapp = Todoapp.new(todoapp_params)

    respond_to do |format|
      if @todoapp.save
        flash[:success] = 'Todoapp was successfully created.'
        format.html { redirect_to @todoapp}
        format.json { render :show, status: :created, location: @todoapp }
      else
        flash[:danger] = 'There was a problem creating the Todo.'
        format.html { render :new }
        format.json { render json: @todoapp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /todoapps/1
  # PATCH/PUT /todoapps/1.json
  def update
    respond_to do |format|
      if @todoapp.update(todoapp_params)
        flash[:success] = 'Todoapp was successfully updated.'
        format.html { redirect_to @todoapp}
        format.json { render :show, status: :ok, location: @todoapp }
      else
        flash[:danger] = 'There was a problem updating the Todo.'
        format.html { render :edit }
        format.json { render json: @todoapp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /todoapps/1
  # DELETE /todoapps/1.json
  def destroy
    @todoapp.destroy
    respond_to do |format|
      flash[:success] = 'Todo was successfully destroyed.'
      format.html { redirect_to todoapps_url}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todoapp
      @todoapp = Todoapp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todoapp_params
      params.require(:todoapp).permit(:title, :notes)
    end
end
