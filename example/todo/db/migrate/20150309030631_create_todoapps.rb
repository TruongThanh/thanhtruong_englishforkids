class CreateTodoapps < ActiveRecord::Migration
  def change
    create_table :todoapps do |t|
      t.string :title
      t.text :notes

      t.timestamps null: false
    end
  end
end
