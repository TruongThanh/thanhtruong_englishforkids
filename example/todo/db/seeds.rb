# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Todoapp.create!(title: 'grocery shopping', notes: 'pickles, eggs, red onion')
Todoapp.create!(title: 'wash the car')
Todoapp.create!(title: 'register kids for school', notes: 'Register Kira for Ruby Junior High and Caleb for Rails High School')
Todoapp.create!(title: 'check engine light', notes: 'The check engine light is on in the Tacoma')
Todoapp.create!(title: 'dog groomers', notes: 'Take Pinky and Redford to the groomers on Wednesday the 23rd')
