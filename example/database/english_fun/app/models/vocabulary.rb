class Vocabulary < ActiveRecord::Base
  belongs_to :quiz
  has_many :details
  belongs_to :study
end
