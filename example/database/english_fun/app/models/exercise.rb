class Exercise < ActiveRecord::Base
  has_one :pronunciation
  has_many :details
  belongs_to :part
end
