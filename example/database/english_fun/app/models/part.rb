class Part < ActiveRecord::Base
  belongs_to :study
  has_many :exercises
end
