class Study < ActiveRecord::Base
  belongs_to :quiz
  has_many :vocabularies
  has_many :parts
  has_many :entertaiments
end
