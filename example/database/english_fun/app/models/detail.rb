class Detail < ActiveRecord::Base
  belongs_to :pronunciation
  belongs_to :exercise
  belongs_to :quiz
  belongs_to :vocabulary
end
