class Quiz < ActiveRecord::Base
  has_one :pronunciation
  has_one :study
  has_one :vocabulary
  has_many :details
end
