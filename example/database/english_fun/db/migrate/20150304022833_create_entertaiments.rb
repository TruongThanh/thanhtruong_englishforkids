class CreateEntertaiments < ActiveRecord::Migration
  def change
    create_table :entertaiments do |t|
      t.string :source
      t.string :img
      t.text :trans
      t.text :content
      t.string :role

      t.timestamps null: false
    end
  end
end
