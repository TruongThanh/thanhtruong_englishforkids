class AddVocabularyIdToDetails < ActiveRecord::Migration
  def change
    add_column :details, :vocabulary_id, :integer
  end
end
