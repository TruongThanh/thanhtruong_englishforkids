class AddPronunciationIdToDetails < ActiveRecord::Migration
  def change
    add_column :details, :pronunciation_id, :integer
  end
end
