class AddQuizIdToPronunciations < ActiveRecord::Migration
  def change
    add_column :pronunciations, :quiz_id, :integer
  end
end
