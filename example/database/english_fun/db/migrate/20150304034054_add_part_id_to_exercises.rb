class AddPartIdToExercises < ActiveRecord::Migration
  def change
    add_column :exercises, :part_id, :integer
  end
end
