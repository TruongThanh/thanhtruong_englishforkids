class AddStudyIdToParts < ActiveRecord::Migration
  def change
    add_column :parts, :study_id, :integer
  end
end
