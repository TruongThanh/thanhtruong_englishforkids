class CreatePronunciations < ActiveRecord::Migration
  def change
    create_table :pronunciations do |t|
      t.integer :exercise_id
      t.string :title

      t.timestamps null: false
    end
  end
end
