class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.string :title
      t.text :content
      t.text :source
      t.string :img
      t.text :trans

      t.timestamps null: false
    end
  end
end
