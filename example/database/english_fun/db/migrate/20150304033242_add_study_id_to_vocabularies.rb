class AddStudyIdToVocabularies < ActiveRecord::Migration
  def change
    add_column :vocabularies, :study_id, :integer
  end
end
