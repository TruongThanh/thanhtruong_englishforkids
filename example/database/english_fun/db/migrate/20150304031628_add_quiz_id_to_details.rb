class AddQuizIdToDetails < ActiveRecord::Migration
  def change
    add_column :details, :quiz_id, :integer
  end
end
