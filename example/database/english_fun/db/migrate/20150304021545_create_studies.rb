class CreateStudies < ActiveRecord::Migration
  def change
    create_table :studies do |t|
      t.integer :quiz_id
      t.string :title

      t.timestamps null: false
    end
  end
end
