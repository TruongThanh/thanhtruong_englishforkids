class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
      t.text :content
      t.string :img
      t.string :source
      t.text :trans

      t.timestamps null: false
    end
  end
end
