class AddExerciseIdToDetails < ActiveRecord::Migration
  def change
    add_column :details, :exercise_id, :integer
  end
end
