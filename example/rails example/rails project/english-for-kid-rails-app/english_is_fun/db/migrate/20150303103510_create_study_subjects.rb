class CreateStudySubjects < ActiveRecord::Migration
  def change
    create_table :study_subjects do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
