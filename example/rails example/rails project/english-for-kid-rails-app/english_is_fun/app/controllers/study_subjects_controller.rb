class StudySubjectsController < ApplicationController
  before_action :set_study_subject, only: [:show, :edit, :update, :destroy]

  # GET /study_subjects
  # GET /study_subjects.json
  def index
    @study_subjects = StudySubject.all
  end

  # GET /study_subjects/1
  # GET /study_subjects/1.json
  def show
  end

  # GET /study_subjects/new
  def new
    @study_subject = StudySubject.new
  end

  # GET /study_subjects/1/edit
  def edit
  end

  # POST /study_subjects
  # POST /study_subjects.json
  def create
    @study_subject = StudySubject.new(study_subject_params)

    respond_to do |format|
      if @study_subject.save
        format.html { redirect_to @study_subject, notice: 'Study subject was successfully created.' }
        format.json { render :show, status: :created, location: @study_subject }
      else
        format.html { render :new }
        format.json { render json: @study_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /study_subjects/1
  # PATCH/PUT /study_subjects/1.json
  def update
    respond_to do |format|
      if @study_subject.update(study_subject_params)
        format.html { redirect_to @study_subject, notice: 'Study subject was successfully updated.' }
        format.json { render :show, status: :ok, location: @study_subject }
      else
        format.html { render :edit }
        format.json { render json: @study_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /study_subjects/1
  # DELETE /study_subjects/1.json
  def destroy
    @study_subject.destroy
    respond_to do |format|
      format.html { redirect_to study_subjects_url, notice: 'Study subject was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_study_subject
      @study_subject = StudySubject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def study_subject_params
      params.require(:study_subject).permit(:title)
    end
end
