json.array!(@study_subjects) do |study_subject|
  json.extract! study_subject, :id, :title
  json.url study_subject_url(study_subject, format: :json)
end
