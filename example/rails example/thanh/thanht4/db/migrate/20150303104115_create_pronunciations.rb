class CreatePronunciations < ActiveRecord::Migration
  def change
    create_table :pronunciations do |t|
      t.string :title
      t.timestamps
    end
  end
end
