json.array!(@pronunciations) do |pronunciation|
  json.extract! pronunciation, :id, :title
  json.url pronunciation_url(pronunciation, format: :json)
end
