require 'test_helper'

class PronunciationsControllerTest < ActionController::TestCase
  setup do
    @pronunciation = pronunciations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pronunciations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pronunciation" do
    assert_difference('Pronunciation.count') do
      post :create, pronunciation: { title: @pronunciation.title }
    end

    assert_redirected_to pronunciation_path(assigns(:pronunciation))
  end

  test "should show pronunciation" do
    get :show, id: @pronunciation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pronunciation
    assert_response :success
  end

  test "should update pronunciation" do
    patch :update, id: @pronunciation, pronunciation: { title: @pronunciation.title }
    assert_redirected_to pronunciation_path(assigns(:pronunciation))
  end

  test "should destroy pronunciation" do
    assert_difference('Pronunciation.count', -1) do
      delete :destroy, id: @pronunciation
    end

    assert_redirected_to pronunciations_path
  end
end
