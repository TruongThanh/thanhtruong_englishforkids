$(document).ready(function(e) {
var next_slide = function() {
    if ($('.slider div[class=current]').next('div').length != 0) {
    $('.slider div[class=current]').next('div').addClass('current');
    $('.slider div[class=current]').prev('div').removeClass('current');
  } else {
    $('.slider div[class=current]').removeClass('current');
    $('.slider div:first-child').addClass('current');
  }
  // Set Remaining Time back to 5 secs (in ms)
  clearTimeout(timeout);
  timeout = setTimeout(next_slide, 50000);
};

// Initial Timer: 5 secs (in ms)
timeout = setTimeout(next_slide, 50000);

// Selects Next Slide (see Next Slide Function)
$('.slider div').click(next_slide);
$('.controls .btnnext').click(next_slide);

// Selects Previous Slide
$('.controls .btnprev').click(function(){
  if ($('.slider div[class=current]').prev('div').length != 0) {
    $('.slider div[class=current]').prev('div').addClass('current');
    $('.slider div[class=current]').next('div').removeClass('current');
  } else {
    $('.slider div[class=current]').removeClass('current');
    $('.slider div:last-child').addClass('current');
  }
});
});