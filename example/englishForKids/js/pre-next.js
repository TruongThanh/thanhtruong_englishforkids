	
	jQuery(document).ready(function(){
		
		var prev = $("input.prev");
		var next = $("input.next");
		var art = $("article");

		var wid = $("body").width();
		prev.css({"marginLeft":wid/2 - 205});

		$(window).resize(function(){
			wid = $("body").width();
			prev.css({"marginLeft":wid/2 - 205});
		});

		next.click(function(){		
			art.animate({
				"left":"-=150px"
				}, {
				duration: 1000,
				complete: function() {
					art.removeAttr("style");
					var sec = $("article section:first-child");
					art.append(sec);
				}
			});
		});

		prev.click(function(){
			art.css({"left":"-=150px"});
			var sec = $("article section:last-child");
			art.prepend(sec);
			art.animate({
				"left":"+=150px"
				}, {
				duration: 1000,
				complete: function() {
					art.removeAttr("style");
				}
			});
		});
	});

