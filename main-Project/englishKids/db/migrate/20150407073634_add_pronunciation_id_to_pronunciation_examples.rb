class AddPronunciationIdToPronunciationExamples < ActiveRecord::Migration
  def change
    add_column :pronunciation_examples, :pronunciation_id, :integer
  end
end
