class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.string :index
      t.string :position
      t.string :source
      t.string :img
      t.string :translate

      t.timestamps null: false
    end
  end
end
