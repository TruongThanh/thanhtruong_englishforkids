class AddSubjectIdToDetails < ActiveRecord::Migration
  def change
    add_column :details, :subject_id, :integer
  end
end
