class CreateVocabularyExamples < ActiveRecord::Migration
  def change
    create_table :vocabulary_examples do |t|
      t.string :index
      t.string :word
      t.string :translate
      t.string :img
      t.string :source

      t.timestamps null: false
    end
  end
end
