class CreateSubQuizzes < ActiveRecord::Migration
  def change
    create_table :sub_quizzes do |t|
      t.string :index
      t.string :answer

      t.timestamps null: false
    end
  end
end
