class QuizzesController < ApplicationController
	layout 'layout'
  def index
  end
  def show
  	@quizzes = Quiz.where(:vocabulary_id => params[:id]).all.paginate(:page => params[:page], per_page: 1)
  	gon.quizzes = Quiz.where(:vocabulary_id => params[:id]).all
  	@sub_quizzes = SubQuiz.all
  	gon.sub_quizzes = SubQuiz.all
  end
end
