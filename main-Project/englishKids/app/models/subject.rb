class Subject < ActiveRecord::Base
	has_many :details
	has_many :quizzes
end
