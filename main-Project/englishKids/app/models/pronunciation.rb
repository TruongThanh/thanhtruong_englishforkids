class Pronunciation < ActiveRecord::Base
	has_many :pronunciation_examples
	has_many :quizzes
end
