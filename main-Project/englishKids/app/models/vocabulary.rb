class Vocabulary < ActiveRecord::Base
	has_many :vocabulary_examples
	has_many :quizzes
end
